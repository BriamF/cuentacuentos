package com.itsm.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView

class Story4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story4)

        val home: ImageView = findViewById(R.id.titleHome)
        val anterior: TextView = findViewById(R.id.anterior4)

        home.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
        }

        anterior.setOnClickListener {
            startActivity(Intent(this, Story3::class.java))
        }
    }
}