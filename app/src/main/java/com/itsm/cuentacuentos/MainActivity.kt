package com.itsm.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val cochinitos:CardView = findViewById(R.id.cvCochinitos)
        val caperucita:CardView = findViewById(R.id.cvCaperucita)
        val tvCorreo: TextView = findViewById(R.id.tvCorreo)
        val salir: TextView = findViewById(R.id.cerrar_sesion)

        cvCochinitos.setOnClickListener{
            startActivity(Intent(this,Story1::class.java))
        }

        cvCaperucita.setOnClickListener {
            Toast.makeText(this, "Proximamente disponible!!!",Toast.LENGTH_SHORT).show()
        }

        salir.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java))
        }

        val correo: String = intent.extras!!.getString("CORREO").toString()
        tvCorreo.text = correo
    }
}