package com.itsm.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlin.math.log

class RegisterActivity : AppCompatActivity() {
    var datos = arrayOfNulls<String>(7)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val spinner: Spinner = findViewById(R.id.spSexo)
        val register: Button = findViewById(R.id.btnRegistrar)




        ArrayAdapter.createFromResource(
            this,
            R.array.sexo,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }



        register.setOnClickListener{
            obtenerValores()
            var contar: Int = 7
            for (i in datos.indices) {
                print(datos[i])
                if (datos[i] != ""){
                    contar--
                }
            }
            if(contar == 0){
                Toast.makeText(this, "Registro completado",Toast.LENGTH_SHORT).show()
                iniciarLogin()
            }else
                Toast.makeText(this, "Debes ingresar todos los datos",Toast.LENGTH_SHORT).show()

        }

    }

    private fun iniciarLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun obtenerValores() {
        val nombre: EditText = findViewById(R.id.edtNombre)
        val apellidos: EditText = findViewById(R.id.edtApellido)
        val sexo: Spinner = findViewById(R.id.spSexo)
        val edad: EditText = findViewById(R.id.edtEdad)
        val correo: EditText = findViewById(R.id.edtNombre)
        val contrasena1: EditText = findViewById(R.id.edtContrasena1)
        val contrasena2: EditText = findViewById(R.id.edtContrasena2)

        datos[0] = nombre.text.toString()
        datos[1] = apellidos.text.toString()
        datos[2] = sexo.selectedItem.toString()
        datos[3] = edad.text.toString()
        datos[4] = correo.text.toString()
        datos[5] = contrasena1.text.toString()
        datos[6] = contrasena2.text.toString()

        print(datos[2])



    }
}