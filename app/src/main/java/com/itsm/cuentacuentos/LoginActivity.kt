package com.itsm.cuentacuentos

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class LoginActivity : AppCompatActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val registrate: TextView = findViewById(R.id.tvRegistrate)
        var ingresar: Button = findViewById(R.id.btnIngresar)

        val correo: EditText = findViewById(R.id.edtCorreo)
        val contrasena: EditText = findViewById(R.id.edtContrasena)

        registrate.setOnClickListener {
            val intent = startActivity(Intent(this, RegisterActivity::class.java))
        }

        ingresar.setOnClickListener {
            if(correo.text.toString() == "")
                Toast.makeText(this, "Es necesario ingresar el correo", Toast.LENGTH_SHORT).show()
            else if(contrasena.text.toString() == "")
                Toast.makeText(this, "Es necesario ingresar la contraseña", Toast.LENGTH_SHORT).show()
            else{
                val i = Intent(Intent(this, ViewInstructions::class.java))
                i.putExtra("CORREO",correo.text.toString())
                startActivity(i)
                Toast.makeText(this, "Bienvenido!!!", Toast.LENGTH_SHORT).show()
            }

        }

    }
}