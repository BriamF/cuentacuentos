package com.itsm.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_story1.*
import kotlinx.android.synthetic.main.activity_story1.view.*
import org.w3c.dom.Text

class Story1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story1)

        val siguiente: TextView = findViewById(R.id.siguiente1)
        val imPequeño: ImageView = findViewById(R.id.imCPequeño)
        val imMediano: ImageView = findViewById(R.id.imCMediano)
        val imGrande: ImageView = findViewById(R.id.imCGrande)

        siguiente.setOnClickListener{
            startActivity(Intent(this, Story2::class.java))
        }

        imPequeño.setOnClickListener{
            mostrarDialogoPerzonalizado("PEQUEÑO")
        }
        imMediano.setOnClickListener{
            mostrarDialogoPerzonalizado("MEDIANO")
        }
        imGrande.setOnClickListener{
            mostrarDialogoPerzonalizado("GRANDE")
        }

    }

    private fun mostrarDialogoPerzonalizado(nombre: String) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)

        val inflater: LayoutInflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_personalizado,null)

        builder.setView(view)

        val dialog: AlertDialog = builder.create()
        dialog.show()

        val txt: TextView = view.findViewById(R.id.textPersonaje)
        txt.text = "Ingresa el valor del nombre del cerdito " + nombre
        val nombreIngresado: TextView = view.findViewById(R.id.edtNombrePersonaje)

        val btnAceptar: Button = view.findViewById(R.id.btnAceptar)
        btnAceptar.setOnClickListener{
            if(txt != null) {
                if (nombre == "PEQUEÑO")
                    this.tvCPequeño.text = nombreIngresado.text
                else if (nombre == "MEDIANO")
                    this.tvCMediano.text = nombreIngresado.text
                else if (nombre == "GRANDE")
                    this.tvCGrande.text = nombreIngresado.text

                dialog.dismiss()
            }else{
                Toast.makeText(view.context, "Ingresa un valor en el campo", Toast.LENGTH_SHORT).show()
            }
        }
    }
}