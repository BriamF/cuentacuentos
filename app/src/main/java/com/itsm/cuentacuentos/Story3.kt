package com.itsm.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_story1.*
import kotlinx.android.synthetic.main.activity_story3.*

class Story3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story3)

        val siguiente: TextView = findViewById(R.id.siguiente3)
        val anterior: TextView = findViewById(R.id.anterior3)
        val imLobo: ImageView = findViewById(R.id.imLobo)

        siguiente.setOnClickListener{
            startActivity(Intent(this, Story4::class.java))
        }

        anterior.setOnClickListener {
            startActivity(Intent(this, Story2::class.java))
        }

        imLobo.setOnClickListener {
            mostrarDialogoPerzonalizado()
        }

    }

    private fun mostrarDialogoPerzonalizado() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)

        val inflater: LayoutInflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_personalizado,null)

        builder.setView(view)

        val dialog: AlertDialog = builder.create()
        dialog.show()

        val txt: TextView = view.findViewById(R.id.textPersonaje)
        txt.text = "Ingresa el valor del nombre del Lobo Feroz"
        val nombreIngresado: TextView = view.findViewById(R.id.edtNombrePersonaje)

        val btnAceptar: Button = view.findViewById(R.id.btnAceptar)
        btnAceptar.setOnClickListener{
            if(txt != null) {
                this.tvLobo.text = nombreIngresado.text
                dialog.dismiss()
            }else{
                Toast.makeText(view.context, "Ingresa un valor en el campo", Toast.LENGTH_SHORT).show()
            }
        }
    }
}