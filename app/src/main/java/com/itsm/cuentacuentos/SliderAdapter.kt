package com.itsm.cuentacuentos

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class SliderAdapter: PagerAdapter {
    var context: Context
    lateinit var layoutInflater: LayoutInflater
    var tabLayout: TabLayout? = null

    constructor(context: Context) {
        this.context = context
    }

    //Arreglos
    val slide_images: ArrayList<Int> = arrayListOf(R.drawable.ic_undraw_bibliophile_hwqc,R.drawable.ic_book_reading,R.drawable.ic_loving_it)
    val slides_headings: java.util.ArrayList<String> = arrayListOf("DESCUBRE MUNDOS","ELIGE Y CLASIFICA","DISFRUTA DEL CONTENIDO")
    val slides_descs: java.util.ArrayList<String> = arrayListOf(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae vulputate metus, dictum hendrerit elit. Aliquam ultricies, dui sit amet.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae vulputate metus, dictum hendrerit elit. Aliquam ultricies, dui sit amet.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae vulputate metus, dictum hendrerit elit. Aliquam ultricies, dui sit amet.")

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as RelativeLayout
    }

    override fun getCount(): Int {
        return slides_headings.size
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.instruction1_layout,container,false)

        var slideImageView: ImageView = view.findViewById(R.id.slide_image) as ImageView
        var slideHeading: TextView = view.findViewById(R.id.slide_heading) as TextView
        var slideDesc: TextView = view.findViewById(R.id.slide_desc) as TextView

        slideImageView.setImageResource(slide_images[position])
        slideHeading.text = slides_headings[position]
        slideDesc.text = slides_descs[position]


        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }
}