package com.itsm.cuentacuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Story2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story2)

        val siguiente: TextView = findViewById(R.id.siguiente2)
        val anterior: TextView = findViewById(R.id.anterior2)

        siguiente.setOnClickListener{
            startActivity(Intent(this, Story3::class.java))
        }

        anterior.setOnClickListener {
            startActivity(Intent(this, Story1::class.java))
        }
    }
}